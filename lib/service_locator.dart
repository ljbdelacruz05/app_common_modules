import 'package:app_common_modules/util/color_util.dart';
import 'package:app_common_modules/util/network_util.dart';
import 'package:app_dependency_manager/app_dependency_manager.dart';



final GetIt serviceLocator = GetIt.instance;


/// call this in our main function
setupDefault(){
  serviceLocator.registerLazySingleton(() => ColorUtil(false));
  SimpleConnectionChecker checker = SimpleConnectionChecker();
  serviceLocator.registerLazySingleton(() => NetworkUtil(checker));
}

/// before using this method call setupDefault in main.dart
NetworkUtil getNetworkUtil() {
  return GetIt.I<NetworkUtil>();
}

Future<bool> hasInternetConnection() async {
  return await getNetworkUtil().isConnected.last;
}

