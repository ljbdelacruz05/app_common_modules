import 'package:app_dependency_manager/app_dependency_manager.dart';


class MySharedPreferences {
  final _valueController = StreamController<String>.broadcast();
  final SharedPreferences sharedPref;
  MySharedPreferences(this.sharedPref);

  Stream<String> get valueStream => _valueController.stream;

  Future<void> setValue(String key, String value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
    _valueController.sink.add(value);
  }

  Future<String> getValue(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(key) ?? '';
  }

  void dispose() {
    _valueController.close();
  }
}
