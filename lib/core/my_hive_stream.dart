import 'package:app_dependency_manager/app_dependency_manager.dart';

class MyHiveStream {
  final _valueController = StreamController<String>.broadcast();
  final Box<dynamic> hiveBox;
  MyHiveStream(this.hiveBox);

  Stream<String> get valueStream => _valueController.stream;

  Future<void> setValue(String key, String value) async {
    await hiveBox.put(key, value);
    _valueController.sink.add(value);
  }

  Future<String> getValue(String key) async {
    return await hiveBox.get(key) ?? '';
  }

  void dispose() {
    _valueController.close();
  }
}
