


import 'package:app_dependency_manager/app_dependency_manager.dart';

class Failure extends Equatable {
  final String message;
  final String code;
  Failure({this.message = "", this.code = ""});
  @override
  // TODO: implement props
  List<Object> get props => [];

  String getErrorMessage(){
    return "Unable to process service at this time";
  }
}


class EmailValidationFailure extends Failure {}
class InvalidInputFailure extends Failure {}
class ParseAmountFailure extends Failure {}

class LocationPermissionDeniedFailure extends Failure {}
class GeoLocationUtilFailure extends Failure {}
class LocationServiceFailure extends Failure {}
class LocationPermissionDeniedForeverFailure extends Failure {}