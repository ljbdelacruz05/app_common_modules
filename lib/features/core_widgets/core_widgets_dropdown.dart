




import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DropdownCustomizableWithHintAtom extends StatefulWidget{
  final String? initialVal;
  final String hintText;
  final TextStyle? hintStyle;
  final TextStyle? labelStyle;
  var dropdownItems;
  final double padding;
  final double width;
  final double height;
  final double borderWidth;
  final Color borderColor;
  final double borderRadius;
  final Color textFieldFill;
  final Alignment labelAlignment;
  final Function? event;
  

  DropdownCustomizableWithHintAtom({
    Key? key,
    this.initialVal,
    this.dropdownItems,
    this.hintText="",
    this.hintStyle,
    this.labelStyle,
    this.padding=10,
    this.width=0.8,
    this.height=0.05,
    this.borderWidth=0.8,
    this.borderColor=Colors.grey,
    this.borderRadius=10,
    this.textFieldFill=Colors.transparent,
    this.labelAlignment = Alignment.centerLeft,
    this.event,
    }) : 
    super(
      key: key,
      );
  @override
  _DropdownCustomizableWithHintAtom createState() => _DropdownCustomizableWithHintAtom();
}

class _DropdownCustomizableWithHintAtom extends State<DropdownCustomizableWithHintAtom>{
  String? initialVal;
  String hintText = "";
  TextStyle? hintStyle;
  TextStyle? labelStyle;
  Function? event;

  var dropdownItems;
  double padding=10;
  double width=0.8;
  double height=0.05;
  double borderWidth=0.8;
  Color borderColor=Colors.grey;
  double borderRadius=10;
  Color textFieldFill=Colors.transparent;
  Alignment labelAlignment = Alignment.centerLeft;

  @override
  void initState() {
    super.initState();
    initialVal = widget.initialVal;
    dropdownItems = widget.dropdownItems;
    hintText = widget.hintText;
    hintStyle = widget.hintStyle;
    labelStyle = widget.labelStyle;
    padding=widget.padding;
    width=widget.width;
    height=widget.height;
    borderWidth=widget.borderWidth;
    borderColor=widget.borderColor;
    borderRadius=widget.borderRadius;
    textFieldFill=widget.textFieldFill;
    labelAlignment = widget.labelAlignment;
    event = widget.event;
  }

  @override
  Widget build(BuildContext context) {
  
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: EdgeInsets.all(padding),
            width: MediaQuery.of(context).size.width * width,
            height: MediaQuery.of(context).size.height * height,
            decoration: BoxDecoration(
              border: Border.all(width: borderWidth, color: borderColor),
              borderRadius: BorderRadius.circular(borderRadius),
              color: textFieldFill,
          ),
          child: Align(
            alignment: labelAlignment,
            child: DropdownButton<String>(
          hint: Text(hintText,
            style: hintStyle,
          ),
          value: initialVal,
          isExpanded: true,
          icon: Icon(Icons.arrow_drop_down),
          iconSize: 25,
          elevation: 16,
          style: labelStyle,
          underline: Container(
            height:3,
          ),
          onChanged: (String? newValue){
            setState(() {
              initialVal = newValue;
              event!();
            });
        },
          items:dropdownItems,
        ),
          ),
        ),
      ],
    );
  }


}