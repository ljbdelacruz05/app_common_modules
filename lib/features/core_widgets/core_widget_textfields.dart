import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class EmptyTextFieldFormAtom extends StatelessWidget {
  final TextEditingController controller;
  final TextInputType keyboard;
  final bool isObscure;
  final TextStyle textStyle;
  final Color fillColor;
  final String hint;
  final Color focusColor;
  final Color cursorColor;
  final Function onChange;
  final int charLimit;
  final bool isEnabled;
  final bool autoFocus;
  final List<TextInputFormatter> formatters;

  EmptyTextFieldFormAtom(
      {
      required this.controller,
      required this.onChange,
      this.textStyle = const TextStyle(fontSize: 15),
      this.keyboard = TextInputType.text,
      this.isObscure = false,
      this.fillColor = Colors.black,
      this.hint = "",
      this.focusColor = Colors.black,
      this.cursorColor = Colors.black,
      this.charLimit = 0,
      this.isEnabled = true,
      this.autoFocus = false,
      this.formatters = const []});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    List<TextInputFormatter> format = [];
    if (formatters != null) {
      format.add(formatters.first);
    }
    return TextFormField(
        onChanged: (s) {
          if (onChange != null) {
            onChange();
          }
        },
        inputFormatters: formatters,
        maxLength: charLimit == 0 ? null : charLimit,
        keyboardType: keyboard,
        style: textStyle,
        controller: controller,
        obscureText: isObscure,
        cursorColor: cursorColor,
        autofocus: autoFocus,
        enabled: isEnabled,
        decoration: InputDecoration(
          labelText: hint,
          counterText: "",
          border: InputBorder.none,
          fillColor: fillColor,
          labelStyle: textStyle,
          focusColor: focusColor,
        ));
  }
}


class TextFieldCustomizableWithHintAtom extends StatelessWidget{
  final EdgeInsetsGeometry padding;
  final double width;
  final double height;
  final double borderWidth;
  final Color? borderColor;
  final double borderRadius;
  final Color textFieldFill;
  final Alignment labelAlignment;
  final String hintText;
  final TextStyle hintStyle;
  final TextStyle inputStyle;
  final bool isObscure;
  final TextEditingController? controller;
  final TextAlign textAlign;
  final bool isEnabled;
  final Function onChange;
  final TextInputType keyboardType;

  TextFieldCustomizableWithHintAtom({
    required this.controller,
    required this.onChange,
    this.keyboardType = TextInputType.text,
    this.padding = const EdgeInsets.all(10),
    this.width=0.8,
    this.height=0.05,
    this.borderWidth=0.8,
    this.borderColor=Colors.grey,
    this.borderRadius=10,
    this.textFieldFill=Colors.transparent,
    this.labelAlignment = Alignment.centerLeft,
    this.hintText="",
    this.inputStyle = const TextStyle(color: Colors.black,),
    this.hintStyle = const TextStyle(color: Colors.black,),
    this.isObscure=false,
    this.textAlign = TextAlign.left,
    this.isEnabled = true,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
            padding: padding,
            width: width,
            height: height,
            decoration: BoxDecoration(
              border: Border.all(width: borderWidth, color: borderColor?? Colors.grey),
              borderRadius: BorderRadius.circular(borderRadius),
              color: textFieldFill,
            ),
            alignment: labelAlignment,
            child: TextField(
              keyboardType: keyboardType,
              controller: controller,
              obscureText: isObscure,
              onChanged: (value) {
                onChange();
              },
              style: inputStyle,
              decoration: InputDecoration.collapsed(
                hintText: hintText,
                hintStyle:hintStyle 
              ),
    ));
  }
}


class CashinFormInputAtom extends StatelessWidget {
  final TextEditingController controller;
  final bool isObscure;
  final String lbl;
  final TextStyle lblStyle;
  final TextStyle tfStyle;
  final double width;
  final double cHeight;
  final double tfHeight;
  final Color borderColor;
  final Function onChangeEvent;
  final bool isEnabled;
  final List<TextInputFormatter> formatters;
  final EdgeInsetsGeometry padding;
  final TextInputType keyboardType;
  CashinFormInputAtom({
    required this.controller, 
    required this.onChangeEvent, 
    this.keyboardType = TextInputType.number,
    this.padding = const EdgeInsets.all(10),
    this.isObscure=false, 
    this.lbl="", 
    required this.lblStyle, 
    required this.tfStyle,
    this.width = 100, 
    this.borderColor=Colors.white, 
    this.isEnabled=true, 
    this.formatters = const [],
    this.cHeight = 50,
    this.tfHeight = 40
    });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children:[
    Text(lbl, style: lblStyle),
    Container(
              width:width,
              height:cHeight,
              decoration: BoxDecoration(
                border: Border.all(color:borderColor),
                color: Colors.transparent,
              ),
              child: Padding(padding:padding,child:
              Row(children:[
                Container(
                  width:MediaQuery.of(context).size.width - 112,
                  height: tfHeight,
                  child:TextField(
                  enabled: isEnabled,
                  keyboardType: keyboardType,
                  inputFormatters: formatters,
                  onChanged: (s){
                    onChangeEvent();
                  },
                  style: tfStyle,
                  controller: controller,
                  obscureText: isObscure,
                  cursorColor: borderColor,
                  decoration: InputDecoration(labelText: "", counterText: "", 
                  border: InputBorder.none,
                  fillColor: borderColor,
                  labelStyle: TextStyle(color:borderColor),
                  focusColor: borderColor,
                )))
              ])
            )
            )
    ]);
  }
}


class CashinFormInputAtomV2 extends StatelessWidget {
  final TextEditingController controller;
  final bool isObscure;
  final String lbl;
  final TextStyle lblStyle;
  final TextStyle textStyle;

  final double cWidth;
  final double cHeight;
  final double tfHeight;
  final double tfWidth;

  final Color borderColor;
  final Function onChangeEvent;
  final bool isEnabled;
  final List<TextInputFormatter> formatters;
  final double radius;
  final EdgeInsetsGeometry paddingContainer;
  final TextInputType keyboard;


  CashinFormInputAtomV2({
    required this.controller, 
    this.paddingContainer = const EdgeInsets.all(10),
    this.isObscure=false, 
    this.lbl="",
    required this.lblStyle, 
    required this.textStyle,
    this.cWidth = 100, 
    this.borderColor=Colors.white, 
    required this.onChangeEvent, 
    this.isEnabled=true, 
    this.formatters = const [],
    this.radius = 10,
    this.keyboard = TextInputType.text,
    this.cHeight = 50,
    this.tfHeight = 40,
    this.tfWidth = 10
  });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children:[

    Text(lbl, style: lblStyle),
    Container(
              width:cWidth,
              height:cHeight,
              decoration: BoxDecoration(
                border: Border.all(color:borderColor),
                color: Colors.transparent,
                borderRadius: BorderRadius.circular(radius)
              ),
              padding: paddingContainer,
              child: Row(children:[
                Container(
                  width:tfWidth,
                  height: tfHeight,
                  child:TextField(
                  enabled: isEnabled,
                  keyboardType: keyboard,
                  inputFormatters: formatters,
                  onChanged: (s){
                    onChangeEvent();
                  },
                  style: textStyle,
                  controller: controller,
                  obscureText: isObscure,
                  cursorColor: borderColor,
                  decoration: InputDecoration(labelText: "", counterText: "", 
                  border: InputBorder.none,
                  fillColor: borderColor,
                  labelStyle: lblStyle,
                  focusColor: borderColor,
                )))
              ])
            )
    ]);
  }
}

class CoreAmountInputTextFieldAtomV2 extends StatelessWidget{
  final TextEditingController controller;
  final Function onChange;
  final TextInputType inputKeyboard;
  final List<TextInputFormatter> formatters;
  final BoxDecoration decoration;
  final String label;
  final TextStyle lblStyle;
  final String inputValueHint;
  final TextStyle inputValueStyle;
  final String currency;
  final double width;
  final double height;
  
  CoreAmountInputTextFieldAtomV2({ 
    required this.controller, 
    required this.onChange, 
    this.inputKeyboard = TextInputType.text, 
    this.formatters = const [], 
    this.label = "", 
    required this.lblStyle, 
    this.inputValueHint = "", 
    required this.inputValueStyle, 
    required this.width, 
    required this.height, 
    required this.decoration,
    this.currency = "PHP", 
  });
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(label, ),
        SizedBox(height: MediaQuery.of(context).size.height * 0.01,),
        Container(
          width: width ,
          height: height,
          decoration: decoration,
          child: Row(
            children: [
              Text(currency,
                   style: lblStyle),
              EmptyTextFieldFormAtom(controller: controller, textStyle: inputValueStyle, onChange: onChange, formatters: formatters)
            ],
          ),
        ),
      ],
    );
  }
}


class EmptyTextFieldFormNoHintAtom extends StatelessWidget {
  final TextEditingController controller;
  final TextInputType keyboard;
  final bool isObscure;
  final TextStyle textStyle;
  final Color fillColor;
  final String hint;
  final Color focusColor;
  final Color cursorColor;
  final Function onChange;
  final int charLimit;
  final bool isEnabled;
  final InputDecoration decoration;
  final TextStyle labelStyle;
  final FloatingLabelBehavior labelBehavior;
  final String labelTxt;
  final List<TextInputFormatter> formatters;
  final bool autoFocus;

  EmptyTextFieldFormNoHintAtom(
      {
      required this.controller,
      required this.keyboard,
      this.isObscure = false,
      required this.textStyle,
      required this.fillColor,
      this.hint = "",
      required this.focusColor,
      required this.cursorColor,
      required this.onChange,
      this.charLimit = 0,
      this.isEnabled = true,
      required this.decoration,
      required this.labelStyle,
      required this.labelBehavior,
      required this.labelTxt, 
      this.formatters = const [],
       this.autoFocus = false});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return TextFormField(
                      onChanged: (s){
                          onChange();
                      },
                      autofocus: autoFocus,
                      inputFormatters: formatters,
                      maxLength: charLimit == 0 ? null : charLimit,
                      keyboardType: keyboard,
                      textInputAction: TextInputAction.done,
                      style: textStyle,
                      controller: controller,
                      obscureText: isObscure,
                      cursorColor: cursorColor,
                      enabled: isEnabled,
                      decoration: decoration              
              );
  }
}
