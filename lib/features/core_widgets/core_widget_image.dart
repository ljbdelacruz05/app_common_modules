import 'package:app_dependency_manager/app_dependency_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ImageAssetWithContainerAtomV2 extends StatelessWidget{
  final double height;
  final double width;
  final EdgeInsetsGeometry? padding;
  final String? image;
  final BoxFit? fit;

  ImageAssetWithContainerAtomV2({
    this.height = 100, 
    this.width = 100, 
    required this.image, 
    required this.padding, 
    this.fit = BoxFit.fill
  });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: height,
      width: width,
      padding: padding,
      child: FittedBox(
        fit: fit??BoxFit.contain,
        child: Image.asset(image!),
      ),
    );
  }
}

class SvgAssetWithContainerAtomV2 extends StatelessWidget{
  final double height;
  final double width;
  final EdgeInsetsGeometry padding;
  final String image;
  final BoxFit fit;

  SvgAssetWithContainerAtomV2({
    this.height = 100, 
    this.width = 100, 
    required this.image, 
    required this.padding, 
    this.fit = BoxFit.fill});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: height,
      width: width,
      padding: padding,
      child: FittedBox(
        fit: fit,
        child: SvgPicture.asset(image),
      ),
    );
  }
}

class CoreMyNetworkImageAtom extends StatelessWidget{
  final String src;
  final double radius;
  final double imageSize;
  final double imageWidthSize;
  final double imageHeightSize;
  final BoxFit fit;
  CoreMyNetworkImageAtom(this.src, this.radius, this.imageSize, {
    this.imageWidthSize = 100,
    this.imageHeightSize = 100,
    this.fit = BoxFit.fill
  });
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ClipRRect(
      borderRadius: BorderRadius.circular(radius),
      child: Image.network(
        src,
        height: imageHeightSize,
        width: imageWidthSize,
        fit: fit,
      ),
    );
  }
}