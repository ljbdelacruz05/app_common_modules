import 'package:app_dependency_manager/app_dependency_manager.dart';
import 'package:flutter/widgets.dart';


/// lottieAnim - resources of the lottie animation path in assets
class LottieAtom extends StatelessWidget {
  final double? width;
  final double? height;
  final String? lottieAnim;
  LottieAtom({
    required this.lottieAnim,
    this.width = 30,
    this.height = 30
  });
  @override
  Widget build(BuildContext context) {
    return Container(
        width: width,
        height: height,
        child: Lottie.asset(lottieAnim!,
            fit: BoxFit.cover, onLoaded: (composition) {
          // controller..duration = composition.duration..repeat();
        }));
  }
}