import 'package:app_dependency_manager/app_dependency_manager.dart';
import 'package:flutter/material.dart';
import 'core_dropdown_bloc.dart';

class CoreDropdownTypePickers extends StatelessWidget {
  final CoreDropDownBloc bloc = CoreDropDownBloc();
  final Color iconColor;
  final Function event;
  final double width;
  final double height;
  final EdgeInsetsGeometry padding;
  final Decoration? decoration;

  final String lblOption;
  final TextStyle textLblStyle;
  CoreDropdownTypePickers(
    {
    required this.event, 
    this.padding = const EdgeInsets.all(10),
    this.decoration,
    this.width = 30, 
    this.lblOption = "", 
    this.height = 30,
    this.iconColor = Colors.black,
    this.textLblStyle = const TextStyle()
    }
  );
  
  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: bloc,
      listener: (context, state) {

      },
      child: BlocBuilder(
      bloc:  bloc,
      builder: (context, state) {
        if(state is CoreDropDownInitialState){
          return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                Container(
                    padding: padding,
                    width: width,
                    height: height,
                    alignment: Alignment.center,
                    decoration: decoration ?? BoxDecoration(
                      border: Border.all(width: 0.8, color: Colors.black54),
                      borderRadius: BorderRadius.circular(5)
                    ),
                    child: DropdownButton<String>(
                    iconEnabledColor: iconColor,
                    hint:Text(lblOption,
                      style: textLblStyle,
                      ),
                    elevation: 0,
                    isExpanded: true,
                    // underline: SizedBox(),
                    value:state.value,
                    onChanged: (selectedType) {
                      event(selectedType);
                      bloc.toInitialState(state.options, selectedType ?? "");
                    },
                    items:
                    state.options.map((type) => DropdownMenuItem<String>(
                            value: type,
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              child: Text(type,
                                          style: textLblStyle
                                          ),
                            ),
                          ))
                      .toList()

                        
                  ))
                    ]
              );
                
        } else {
          return Shimmer.fromColors(child: Container(
                    padding: padding,
                    width: width,
                    height: height,
                    alignment: Alignment.center,
                    decoration: decoration ?? BoxDecoration(
                        border: Border.all(width: 0.8, color: Colors.black54),
                        borderRadius: BorderRadius.circular(5)
                      ),
                    ), 
                    baseColor: iconColor, highlightColor: iconColor
          );
        }
      },
    ),
    );
  }
}