import 'package:app_dependency_manager/app_dependency_manager.dart';


abstract class CoreDropDownState {}
abstract class CoreDropDownEvent {}


class CoreDropDownInitialState extends CoreDropDownState {
  final List<String> options;
  final String value;
  CoreDropDownInitialState(this.options, this.value);
}
class CoreDropDownLoadingState extends CoreDropDownState {}
class CoreDropDownDisabledState extends CoreDropDownState {}

/// this bloc controls the drop down state of the widget
/// 
/// 
/// Features:
/// 
/// call toInitialState method to show the options and the selected default value of the widget this is called everytime users select the option
/// 
/// 

class CoreDropDownBloc extends Bloc<CoreDropDownEvent, CoreDropDownState> {
  CoreDropDownBloc():super(CoreDropDownLoadingState());
  toLoading(){
    if(!(state is CoreDropDownLoadingState)){
      emit(CoreDropDownLoadingState());
    }
  }

  toInitialState(List<String> options, String value){
    toLoading();
    emit(CoreDropDownInitialState(options, value));
  }

}


