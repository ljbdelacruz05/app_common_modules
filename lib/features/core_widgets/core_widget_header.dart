import 'package:flutter/material.dart';


class CoreHeaderAtom extends StatelessWidget{
  final String header;
  final Function? event;
  final double height;
  final Color iconColor;
  final double iconSz;
  final TextStyle? titleStyle;
  final Widget? icon;
  final Color? bgColor;
  final EdgeInsetsGeometry padding;

  CoreHeaderAtom(
      {this.header = "",
      this.event,
      this.height = 50,
      this.iconColor = Colors.white,
      this.titleStyle,
      this.iconSz = 20,
      this.icon,
      this.bgColor = Colors.white,
      this.padding = const EdgeInsets.only()
      });

  @override
  Widget build(BuildContext context) {
    return Container(
        color: bgColor,
        padding: padding,
        width: MediaQuery.of(context).size.width,
        height: height,
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              InkWell(
                  onTap: () {
                    if (event != null) {
                      event!();
                    }
                  },
                  child: icon??Icon(Icons.arrow_back_ios,
                      color: Colors.white,
                      size: iconSz)
              ),
              Text(header, style: titleStyle)
            ]));
  }
}


class CoreHeaderSubmit1Mol extends StatelessWidget {
  final String header;
  final Function? event;
  final double height;
  final Color iconColor;
  final double iconSz;
  final TextStyle? titleStyle;
  final String submitLbl;
  final Function? submitEvent;
  final Color headerBGColor;
  final Color textColor;
  final bool isShowSubmit;
  final EdgeInsetsGeometry padding;
  CoreHeaderSubmit1Mol(
      {this.header = "",
      this.event,
      this.height = 50,
      this.iconColor = Colors.white,
      this.titleStyle,
      this.iconSz = 20,
      this.submitLbl="",
      this.submitEvent,
      this.headerBGColor = Colors.white,
      this.textColor = Colors.white,
      this.isShowSubmit = false,
      this.padding = const EdgeInsets.only()
  });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        color: headerBGColor,
        padding: padding,
        width: MediaQuery.of(context).size.width,
        height: height,
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              InkWell(
                  onTap: () {
                    if (event != null) {
                      event!();
                    }
                  },
                  child: Icon(Icons.arrow_back_ios,
                      color: iconColor,
                      size: iconSz)),
              Text(header, style: titleStyle),
              submitBtn()
            ]));
  }

  Widget submitBtn(){
      if(isShowSubmit){
        return InkWell(onTap:(){
          if(submitEvent != null){
            submitEvent!();
          }
        }, child: Text(submitLbl, style: titleStyle));
      }else{
        return Container();
      }
  }
}


class CoreHeaderCloseMol extends StatelessWidget {
  final String header;
  final Function? event;
  final double height;
  final Color iconColor;
  final Color bgColor;
  final double iconSz;
  final TextStyle? titleStyle;
  final EdgeInsetsGeometry padding;
  CoreHeaderCloseMol(
      {this.header = "",
      this.event,
      this.height = 50,
      this.iconColor = Colors.white,
      this.bgColor = Colors.white,
      this.titleStyle,
      this.iconSz = 20,
      this.padding = const EdgeInsets.only(),
      });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        color: bgColor,
        padding: padding,
        width: MediaQuery.of(context).size.width,
        height: height,
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(header, style: titleStyle),
              InkWell(
                  onTap: () {
                    if (event != null) {
                      event!();
                    }
                  },
                  child: Icon(Icons.close,
                      color: iconColor,
                      size: iconSz)),
            ]));
  }
}
