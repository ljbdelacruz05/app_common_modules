



import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CoreReusableContainerwithShadowAtomV2 extends StatelessWidget{
  final double width;
  final double height;
  final double radius;
  final EdgeInsets padding;
  final Widget content;
  final Color color;
  final Color shadowColor;
  final BoxBorder border;
  final Offset offset;
  final double blurRadius;
  final double spreadRadius;
  final bool hasCards;
  final double elevation;
  final bool semanticContainer;

  CoreReusableContainerwithShadowAtomV2({
    required this.width, 
    required this.height, 
    this.radius = 12, 
    required this.content, 
    this.color = Colors.black, 
    required this.padding, 
    this.shadowColor = Colors.black, 
    required this.border,
    this.blurRadius = 2.0,
    this.spreadRadius = 1.0,
    required this.offset,
    this.hasCards = false,
    this.elevation = 10,
    this.semanticContainer = false
  });

  @override
  Widget build(BuildContext context) {
    if(hasCards) {
      return cardContent();
    } else {
      return defaultContent();
    }
  }

  Widget defaultContent(){
    return Container(
            padding: padding,
            width: width,
            height:height,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                    color: shadowColor,
                    offset:  offset,
                    blurRadius: blurRadius,
                    spreadRadius: spreadRadius,
                  ),
              ],
              border: border,
              color: color,
              borderRadius: BorderRadius.circular(
              radius,
            )),
            child: content,
    );
  }

  Widget cardContent(){
    return Card(
          semanticContainer: semanticContainer,
          elevation: elevation,
          color: color,
          shadowColor: shadowColor,
          child: defaultContent(),
    );
  }
}