import 'package:app_dependency_manager/app_dependency_manager.dart';



abstract class DatePickerState {}
abstract class DatePickerEvent {}

class InitialDatePickerFromState extends DatePickerState {}
class LoadingDatePickerFromState extends DatePickerState {}
class ShowDetailsDatePickerFromState extends DatePickerState{
  final DateTime date;
  ShowDetailsDatePickerFromState(this.date);
}


/// this bloc controls the UI date picker, 
class DatePickerBloc extends Bloc<DatePickerEvent, DatePickerState> {
  DatePickerBloc():super(InitialDatePickerFromState());
  toLoading(){
    if(!(state is LoadingDatePickerFromState)){
      emit(LoadingDatePickerFromState());
    }
  }
  pickDate(DateTime date) async {
    toLoading();
    emit(ShowDetailsDatePickerFromState(date));
  }  
}