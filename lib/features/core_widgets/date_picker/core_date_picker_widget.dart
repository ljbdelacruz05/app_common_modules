import 'package:app_dependency_manager/app_dependency_manager.dart';
import 'package:flutter/material.dart';
import 'core_date_picker_bloc.dart';

// reusable date picker
void showCalendarDatePicker(BuildContext context, Function event, DatePickerBloc bloc) async {
    var results = await showCalendarDatePicker2Dialog(
      context: context,
      config: CalendarDatePicker2WithActionButtonsConfig(),
      dialogSize: const Size(325, 400),
      value: [DateTime.now()],
      borderRadius: BorderRadius.circular(15),
    );
    bloc.pickDate(results!.first!);
    event(results.first!);
}

class CoreDatePicker1Widget extends StatelessWidget {
  final DatePickerBloc bloc;
  final TextStyle? lblTextStyle;
  final String? lblText;
  final Color? boxDecorColor;
  final Widget? pickerIcon;
  final double? horizontalSpacing;
  final TextStyle? dateTextStyle;
  final Function event;
  CoreDatePicker1Widget(
    this.bloc,
    this.event, {
    this.lblText = "",
    this.lblTextStyle = const TextStyle(fontSize: 14, color: Colors.black),
    this.boxDecorColor = Colors.black,
    this.pickerIcon = const Icon(Icons.calendar_month, size: 24, color: Colors.black),
    this.horizontalSpacing = 10,
    this.dateTextStyle = const TextStyle(fontSize: 14, color: Colors.black)
  });
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
        return BlocBuilder<DatePickerBloc, DatePickerState>(
          bloc: bloc,
          builder: (context, state) {
            return Row(children:[
                Text(lblText ?? '', style: lblTextStyle),
                SizedBox(width: MediaQuery.of(context).size.width * 0.01,),
                InkWell(onTap:(){
                  showCalendarDatePicker(context, event, bloc);
                }, child: Container(
                  decoration: BoxDecoration(
                    color: boxDecorColor,
                    borderRadius: BorderRadius.circular(10)
                  ),
                  width: MediaQuery.of(context).size.width * 0.05,
                  height: MediaQuery.of(context).size.height * 0.05,
                  child: pickerIcon,
                )
                      
                ),
                SizedBox(width: horizontalSpacing),
                state is ShowDetailsDatePickerFromState ? Text(state.date.year.toString()+"/"+state.date.month.toString()+"/"+state.date.day.toString(),
                                                               style: dateTextStyle,
                ) : Container()
            ]);

    });
  }

}