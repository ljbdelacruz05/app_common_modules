



//INFORMATION,OTHERS
import 'package:flutter/widgets.dart';

class CoreBusinessDisplayTextAtom extends StatelessWidget{
  final EdgeInsetsGeometry? padding;
  final String? text;
  final TextStyle? style;
  CoreBusinessDisplayTextAtom({this.text = "", this.padding = const EdgeInsets.only(), this.style});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: padding,
      child: Text(text ?? "",
              style: style,
              textAlign: TextAlign.center,
            ),
    );
  }
}

class CurrencyAmountTextAtom extends StatelessWidget {
  final TextStyle style;
  final Widget contents;
  final String currency;
  CurrencyAmountTextAtom(this.style, this.contents, {this.currency = "₱",});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(children: [Text(currency, style: style), contents]);
  }
}