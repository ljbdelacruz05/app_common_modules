import 'package:flutter/material.dart';


class CoreDividerAtom extends StatelessWidget {
  final double? width;
  final Color? color;
  final double? thickness;
  
  CoreDividerAtom(
      {this.width = 1, this.color = Colors.white, this.thickness = 1});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        width: width,
        child: Divider(
          color: color,
          thickness: thickness,
        ));
  }
}
