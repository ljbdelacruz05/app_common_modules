



import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CoreThemeBackgroundAtom extends StatelessWidget {
  final double headerHeight;
  final Widget? footer;
  final Widget? drawer;
  final Widget? endDrawer;
  final Widget? header;
  final Color? bgColor;
  final Widget content;
  final Color? appBarColor;
  final GlobalKey<ScaffoldState>? scaffoldKey;
  final double? toolbarHeight;
  CoreThemeBackgroundAtom(this.content,
      {this.header,
      this.headerHeight = 0,
      this.footer,
      this.drawer,
      this.endDrawer,
      this.bgColor = Colors.white,
      this.scaffoldKey, 
      this.appBarColor = Colors.white,
      this.toolbarHeight = 0});
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      key: scaffoldKey,
     appBar: AppBar(
      automaticallyImplyLeading: false,
      title: header, 
      backgroundColor: appBarColor, 
      toolbarHeight: toolbarHeight),
      body: Container(
          color: bgColor,
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: content),
      bottomNavigationBar: footer,
      drawer: drawer,
      endDrawer: endDrawer,
    ));
  }
}

class CoreThemeImageBackgroundAtom extends StatelessWidget {
  final double headerHeight;
  final Widget? footer;
  final Widget? drawer;
  final Widget? endDrawer;
  final Widget? header;
  final Color bgColor;
  final Widget content;
  final String bgImage;
  final GlobalKey<ScaffoldState>? scaffoldKey;

  CoreThemeImageBackgroundAtom(this.content,
      {this.header,
      this.headerHeight = 0,
      this.footer,
      this.drawer,
      this.endDrawer,
      this.bgColor = Colors.white,
      this.scaffoldKey,
      this.bgImage = ""});
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      key: scaffoldKey,

      appBar: PreferredSize(
          preferredSize: const Size.fromHeight(80),
          child: header ?? Container()),
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(bgImage),
              fit: BoxFit.cover,
            ),
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: content),
      bottomNavigationBar: footer,
      drawer: drawer,
      endDrawer: endDrawer,
    ));
  }
}

class CoreThemeImageBackgroundNoHeaderAtom extends StatelessWidget {
  final Widget? footer;
  final Widget? drawer;
  final Widget? endDrawer;
  final Color bgColor;
  final Widget content;
  final String bgImage;
  final GlobalKey<ScaffoldState>? scaffoldKey;

  CoreThemeImageBackgroundNoHeaderAtom(this.content,
      {
      this.footer,
      this.drawer,
      this.endDrawer,
      this.bgColor = Colors.white,
      this.scaffoldKey,
      this.bgImage = ""
      });
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      key: scaffoldKey,
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(bgImage),
              fit: BoxFit.cover,
            ),
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: content),
      bottomNavigationBar: footer,
      drawer: drawer,
      endDrawer: endDrawer,
    ));
  }
}

