import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_stories/flutter_stories.dart';

class CoreWidgetStoryFullScreen extends StatelessWidget{
  final int itemCount;
  final Duration momentDuration;
  final List<String> images;
  final Function doneEvent;
  CoreWidgetStoryFullScreen({
    required this.images,
    required this.doneEvent,
    required this.momentDuration,
    required this.itemCount
  });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CupertinoPageScaffold(
                    child: Story(
                      onFlashForward:(){
                        doneEvent();
                      },
                      onFlashBack: (){
                      },
                      momentCount: itemCount,
                      momentDurationGetter: (idx) => momentDuration,
                      momentBuilder: (context, idx){
                        return Image.asset(images[idx]);
                      },
                    ),
    );
  }
}