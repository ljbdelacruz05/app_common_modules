import 'package:app_dependency_manager/app_dependency_manager.dart';


abstract class CoreButtonState {}
abstract class CoreButtonEvent {}


class CoreButtonInitialState extends CoreButtonState {}
class CoreButtonLoadingState extends CoreButtonState {}
class CoreButtonDisabledState extends CoreButtonState {}

/// this bloc controls the UI button loading and initial state, 
/// 
/// Features:
/// 
/// toInitialState - method show the button lbl etc this button is interactable on this state
/// 
/// toLoadingState - makes the button uninteractable it will show the loading state
class CoreButtonBloc extends Bloc<CoreButtonEvent, CoreButtonState> {
  CoreButtonBloc():super(CoreButtonInitialState());

  toInitialState(){
    emit(CoreButtonInitialState());
  }

  toLoadingState(){
    if(!(state is CoreButtonLoadingState)) {
      emit(CoreButtonLoadingState());
    }
  }

  toDisabledButtonState(){
    emit(CoreButtonDisabledState());
  }

}




