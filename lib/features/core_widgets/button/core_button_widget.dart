import 'package:app_dependency_manager/app_dependency_manager.dart';
import 'package:flutter/material.dart';
import 'core_button_bloc.dart';


class CoreRawMaterialButtonAtom extends StatelessWidget {
  final CoreButtonBloc bloc;
  final Function? onClick;
  final Color bgColor;
  final double radius;
  final double elevation;
  final Widget? content;
  final Color? borderColor;
  final double width;

  CoreRawMaterialButtonAtom({
    required this.bloc,
    this.onClick,
    this.bgColor = Colors.white,
    this.radius = 100,
    this.elevation = 0,
    this.content,
    this.borderColor = Colors.transparent,
    this.width = 100,
  });
  @override
  Widget build(BuildContext context) {

    return BlocBuilder<CoreButtonBloc, CoreButtonState>(
          bloc: bloc,
          builder: (context, state) {

          return RawMaterialButton(
                  onPressed: () {
                    if(state is CoreButtonInitialState) {
                      if (onClick != null) {
                        onClick!();
                      }            
                    }
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(radius)),
                      side: BorderSide(color: borderColor!)),
                  fillColor: bgColor,
                  elevation: elevation,
                  child: state is CoreButtonLoadingState ? const CircularProgressIndicator() : content
          );

    });

  }
}

class CoreImageButtonAtom extends StatelessWidget {
  final CoreButtonBloc bloc;
  final double imageWidth;
  final double imageHeight;
  final String image;
  final Function? onClick;
  final BoxFit fit;
  CoreImageButtonAtom(
      {
        required this.bloc,
        required this.image,
        required this.onClick, 
        this.imageWidth = 0, 
        this.imageHeight = 0, 
        this.fit = BoxFit.fill,
  });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return BlocBuilder<CoreButtonBloc, CoreButtonState>(
          bloc: bloc,
          builder: (context, state) {
      
      if(state is CoreButtonLoadingState) {
        return Container(
          width: imageWidth,
          height: imageHeight,
          child: const CircularProgressIndicator()
        );
      } else {
        return InkWell(
            onTap: () {
              if(state is CoreButtonInitialState){
                if (onClick != null) {
                  onClick!();
                }
              }
            },
            child: Image.asset(image,
                fit: fit, height: imageHeight, width: imageWidth
            )
        );        
      }
    });

  }
}

class CoreTextButtonAtom extends StatelessWidget {
  final CoreButtonBloc bloc;
  final Function onClick;
  final String lbl;
  final TextStyle style;
  CoreTextButtonAtom(
      {
      required this.bloc,  
      required this.onClick,
      required this.lbl,
      required this.style
  });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

 return BlocBuilder<CoreButtonBloc, CoreButtonState>(
          bloc: bloc,
          builder: (context, state) {
      
      if(state is CoreButtonLoadingState) {
        return Container(
          width: 30,
          height: 30,
          child: const CircularProgressIndicator()
        );
      } else {
        return InkWell(
        onTap: () {
          onClick();
        },
        child: Text(lbl,
            style: style
        ));       
      }
    });

    }
  }