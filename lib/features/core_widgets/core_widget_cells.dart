



import 'package:app_dependency_manager/app_dependency_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BillerCategoryCellAtom extends StatelessWidget {
  final Function event;
  final TextStyle style;
  final String lbl;
  final String assetImage;
  final Color bgColor;
  final double width;
  final double height;
  final double imageWidth;
  final double imageHeight;
  final bool isNetwork;
  final EdgeInsetsGeometry padding;
  final EdgeInsetsGeometry paddingLbl;
  final TextAlign textAlign;
  final double radius;
  final bool hasCard;
  final Color borderColor;
  final Color shadowColor;
  final double textScaleFactor;
  final double imageTextSpacing;
  final BoxBorder boxBorder;
  BillerCategoryCellAtom(
      {
      required this.event,
      required this.style,
      this.lbl = "",
      this.assetImage = "",
      this.bgColor = Colors.white,
      this.width = 0,
      this.height = 0,
      this.imageHeight = 0,
      this.imageWidth = 0,
      this.isNetwork = false,
      required this.padding,
      required this.paddingLbl,
      this.textAlign = TextAlign.center,
      this.radius = 10,
      this.hasCard = false,
      this.borderColor = Colors.black,
      this.shadowColor = Colors.black,
      this.textScaleFactor = 0.8,
      this.imageTextSpacing = 20,
      required this.boxBorder
      });

  @override
  Widget build(BuildContext context) {
    if (hasCard) {
      return InkWell(
        onTap: (){
          event();
        },
        child: Column(
          children: [
            Card(
          semanticContainer: false,
          elevation: 10,
          color: Colors.transparent,
          shadowColor: Colors.grey.withOpacity(0.3),
          child: Container(
                  alignment: Alignment.center,
                  width: width,
                  height: height,
                  decoration: BoxDecoration(
                    border: boxBorder,
                    borderRadius: BorderRadius.circular(radius),
                    color: borderColor,
                  ),
                  child: isNetwork
                  ? CachedNetworkImage(
                              imageUrl: assetImage,
                              height: imageHeight,
                              width: imageWidth,

                              )
                  : Image.asset(assetImage,
                              width: imageWidth,
                              height: imageHeight,
                              ),
          ),
        ),
            SizedBox(height: imageTextSpacing),
            Flexible(child: Text(
              lbl,
              textAlign: textAlign,
              style: style,
              textScaleFactor: textScaleFactor,
            ),)
          ],
        ),
      );
    } else {

    }
    return InkWell(
        onTap: () {
          if (event != null) {
            event();
          }
        },
        child: Container(
            width: width,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: padding,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(radius)),
                      color: bgColor,
                    ),
                    child: isNetwork
                        ? CachedNetworkImage(
                            imageUrl: assetImage,
                            height: imageHeight,
                            width: imageWidth)
                        : Image.asset(assetImage,
                            width: imageWidth,
                            height: imageHeight,
                            fit: BoxFit.contain),
                  ),
                  Container(
                      width: width,
                      padding: paddingLbl,
                      child:
                          Text(lbl, textAlign: textAlign, style: style))
                ])));
  }
}


class CoreCellWidget extends StatelessWidget{
  final String label1;
  final String label2;
  final TextStyle labelStyle;
  final Widget icon;
  final Function event;
  final Color  bgCellColor;
  final double width;
  final double height;
  final double radius;
  final Color shadowColor;
  final EdgeInsetsGeometry padding;
  final Color cardColor;
  final double elevation;
  final bool semanticContainer;
  
  CoreCellWidget({
    this.label1 = "",
    this.label2 = "",
    required this.icon,
    required this.event,
    this.bgCellColor = Colors.black,
    required this.labelStyle,
    this.width = 0,
    this.height = 0,
    this.radius = 12,
    this.shadowColor = Colors.black,
    required this.padding,
    this.cardColor = Colors.transparent,
    this.elevation = 10,
    this.semanticContainer = false
  });
  
  Widget build(BuildContext context ){
    return InkWell(
      onTap: (){
        event();
      },
      child: Container(
        child:Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Card(
            semanticContainer: semanticContainer,
            margin: padding,
            color: cardColor,
            elevation: elevation,
            shadowColor: shadowColor,
            child:
            Container(
            width: width,
            height: height,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(radius),
              color: bgCellColor
            ),
            child: icon,
          ),
          ),          
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(label1, style: labelStyle),
              Text(label2, style: labelStyle),
            ],
          ),
        ]
      ),
    ),
    );
  }
}