import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CoreProgressIndicatorAtom extends StatelessWidget{
  final Color color;
  final double height;
  final double width;
  CoreProgressIndicatorAtom({this.color = Colors.blue, this.width = 30, this.height = 30});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: height, 
      width: width,
      child:CircularProgressIndicator(color: color));
  }
}