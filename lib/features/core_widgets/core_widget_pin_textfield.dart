import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';


class PinCodeTextFieldAtom extends StatelessWidget {
  final TextEditingController? controller;
  final int pinCodeLength;
  final double pincodeRadius;
  final Color pinBoxColor;
  final Color hasTextBorderColor;
  final Color defaultBorderColor;
  final Color errorBorderColor;
  final Color highlightColor;
  final Color textColor;
  final Function? getOtp;
  final double? pinBoxWidth;
  final double? pinBoxHeight;
  final String maskChar;
  final bool hideChar;

  PinCodeTextFieldAtom(
      {
        
      required this.controller,
      this.pinCodeLength = 4,
      this.pincodeRadius = 5,
      this.pinBoxColor = Colors.white,
      this.hasTextBorderColor = Colors.grey,
      this.defaultBorderColor = Colors.white,
      this.errorBorderColor = Colors.red,
      this.textColor = Colors.white,
      this.highlightColor = Colors.white,
      required this.getOtp,
      this.pinBoxWidth = 0,
      this.pinBoxHeight = 0,
      this.maskChar = "\u25CF",
      this.hideChar=true,
      
      });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return PinCodeTextField(
        pinBoxBorderWidth: 1, 
          maxLength: pinCodeLength, 
        pinBoxWidth: pinBoxWidth ?? 0 / pinCodeLength,
        pinBoxHeight: pinBoxHeight ?? 0 / pinCodeLength,
        controller: controller,
        pinBoxRadius: pincodeRadius,
        pinBoxColor: pinBoxColor,
        hasTextBorderColor: hasTextBorderColor,
        defaultBorderColor: defaultBorderColor,
        errorBorderColor: errorBorderColor,
        wrapAlignment: WrapAlignment.center,
        highlightPinBoxColor: highlightColor,
        hideCharacter: hideChar,
        maskCharacter: maskChar,
        pinTextStyle: TextStyle(color: textColor),
        onTextChanged: (otp) {
          if (getOtp != null) {
            getOtp!(otp);
          }
        });
  }
}