import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CoreContainerWrapAtomV2 extends StatelessWidget{
  final double width;
  final double height;
  final EdgeInsetsGeometry? padding;
  final double radius;
  final Color bgColor;
  final List<BoxShadow>? boxShadow;
  final Widget? content;

  CoreContainerWrapAtomV2({
    this.width=100, 
    this.height=100, 
    this.padding, 
    this.radius=8, 
    this.bgColor=Colors.white, 
    this.boxShadow, 
    this.content});

  @override
  Widget build(BuildContext context) {
   return Container(
    width: width,
    height: height,
    padding: padding,
    decoration: BoxDecoration(
      color: bgColor,
      borderRadius: BorderRadius.circular(radius),
      boxShadow: boxShadow,
    ),
    child: content,
    );
  }
}