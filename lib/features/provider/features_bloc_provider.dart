import 'package:app_dependency_manager/app_dependency_manager.dart';
import 'package:flutter/material.dart';

import '../core_widgets/button/core_button_bloc.dart';
import '../core_widgets/date_picker/core_date_picker_bloc.dart';

final featuresBloCProviders = [

    // Date Picker
    BlocProvider<DatePickerBloc>(
          create: (BuildContext context) => DatePickerBloc(),
    ),

    // Button
];