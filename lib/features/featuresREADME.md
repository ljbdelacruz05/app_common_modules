Author: ljbdelacruz

# Features README

## Date Picker
DatePickerBloc - 

CoreDatePicker1Widget


## Background / SafeArea / Scaffold
CoreThemeBackgroundAtom - plain background 
CoreThemeImageBackgroundAtom - background with image 
CoreThemeImageBackgroundNoHeaderAtom - background without header


## Button
CoreButtonBloc -> this will modify the button state if loading, or display initial

CoreRawMaterialButtonAtom
CoreImageButtonAtom
CoreTextButtonAtom



```
  final CoreButtonBloc submitButtonBloc = CoreButtonBloc();

  return Column(children:[
    CoreImageButtonAtom(bloc: submitButtonBloc, image: "image source here", onClick: (){
      // event invoked here when button is clicked
    })
  ]);

```


## Cards

CoreContainerWrapAtomV2


## Cells
BillerCategoryCellAtom
CoreCellWidget


## Container

CoreReusableContainerwithShadowAtomV2


## Divider
CoreDividerAtom


## Header
CoreHeaderAtom
CoreHeaderSubmit1Mol
CoreHeaderCloseMol


## Image Display Widget
ImageAssetWithContainerAtomV2
SvgAssetWithContainerAtomV2
CoreMyNetworkImageAtom


## Layout Widget 
FourDisplayWidgetsMol


## PinCode TextFields
PinCodeTextFieldAtom

## Textfields
EmptyTextFieldFormAtom
TextFieldCustomizableWithHintAtom
CashinFormInputAtom
CashinFormInputAtomV2
CoreAmountInputTextFieldAtomV2
EmptyTextFieldFormNoHintAtom



## Progress Widget
CoreProgressIndicatorAtom


## Facebook Story Display Widget
- widgets similar to facebook story display just provide the images that will be displayed on the story and its length
CoreWidgetStoryFullScreen



## Dropdown
CoreDropDownBloc

CoreDropdownTypePickers


## Lottie Widget
LottieAtom









