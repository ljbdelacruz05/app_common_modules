
import 'package:flutter/services.dart';

class WhiteListingUtil {
  List<TextInputFormatter> getNumberOnlyWhiteList() {
    return [FilteringTextInputFormatter.allow(RegExp("[0-9]"))];
  }
  List<TextInputFormatter> getNumberDecimalOnlyWhiteList(){
    return [FilteringTextInputFormatter.allow(RegExp(r"^\d*\.?\d{0,2}"))];
  }
  List<TextInputFormatter> alphabetOnlyWhiteList() {
    return [FilteringTextInputFormatter.allow(RegExp("[a-zA-Z ]"))];
  }
  List<TextInputFormatter> alphabetAndNumberOnlyWhiteList() {
    return [FilteringTextInputFormatter.allow(RegExp("[a-zA-Z-0-9 ]"))];
  }
  List<TextInputFormatter> emailInputWhiteList() {
    return [
      FilteringTextInputFormatter.allow(RegExp(r"^[ A-Za-z0-9_@./#&+-]*$"))
    ];
  }
}