Author: ljbdelacruz

# Utils README

NOTE: if made changes on models need to run this command in order for your changes on the class be automatically generated
flutter pub run build_runner build --delete-conflicting-outputs


## PushNotificationUtil
```
    // Methods Available
    void showPushNotif(String title, String message)
    void showPushNotifFromMessage(String title, String message, PushNotificationPayloadModel payload)
```

## BoxDecorationUtil


## ColorUtil (optional uses)
inject this class in your service locator and initialize


## NetworkUtil

```
    // methods available
    // if you want to return debug mode of dio which will include prettyfy display of the request sent via http or receive else it will not print the request use strictly on debugging mode

    Dio getDio({bool isDebug = false, int maxWidth = 128, bool requestBody = true, bool requestHeader = true, bool request = true, bool error = true, bool responseBody = true, String cert = ""})
    
```

## StringUtil

```
    // methods available
    String amountFormatter(double balance);
    String removeSpecCarFromMobile(String mobile, {String dialCode = "+63"})
    bool isEmail(String input)
```


## ValidationUtil
```
    // methods available
    Future<Either<Failure, Success>> validateEmail(String email)
    Future<Either<Failure, Success>> validatePhonenumber(String phonenumber)
    Future<Either<Failure, Success>> checkAmountIfZero(String amount)
    Future<Either<Failure, Success>> checkIfHasSpecialChar(String text)
```

## WhiteListingUtil

```
    List<TextInputFormatter> getNumberOnlyWhiteList()
    List<TextInputFormatter> getNumberDecimalOnlyWhiteList()
    List<TextInputFormatter> alphabetOnlyWhiteList()
    List<TextInputFormatter> alphabetAndNumberOnlyWhiteList()
    List<TextInputFormatter> emailInputWhiteList()
```





