
import 'package:freezed_annotation/freezed_annotation.dart';

part 'notification_model.freezed.dart';
part 'notification_model.g.dart';


@Freezed()
class PushNotificationPayloadModel with _$PushNotificationPayloadModel {
  const factory PushNotificationPayloadModel(
      {
      required String type, // type - NOTIF - notif only no action, DEEPLINK - redirect to specific page, DEEPLINK_STORE - redirect to certain store, DEEPLINK_PRODUCT - deep link redirect to the product details, MESSAGE
      String? data,
      String? feature
      }) = _PushNotificationPayloadModel;

  factory PushNotificationPayloadModel.fromJson(Map<String, dynamic> json) =>
      _$PushNotificationPayloadModelFromJson(json);
}




