// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PushNotificationPayloadModel _$$_PushNotificationPayloadModelFromJson(
        Map<String, dynamic> json) =>
    _$_PushNotificationPayloadModel(
      type: json['type'] as String,
      data: json['data'] as String?,
      feature: json['feature'] as String?,
    );

Map<String, dynamic> _$$_PushNotificationPayloadModelToJson(
        _$_PushNotificationPayloadModel instance) =>
    <String, dynamic>{
      'type': instance.type,
      'data': instance.data,
      'feature': instance.feature,
    };
