// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'notification_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PushNotificationPayloadModel _$PushNotificationPayloadModelFromJson(
    Map<String, dynamic> json) {
  return _PushNotificationPayloadModel.fromJson(json);
}

/// @nodoc
mixin _$PushNotificationPayloadModel {
  String get type =>
      throw _privateConstructorUsedError; // type - NOTIF - notif only no action, DEEPLINK - redirect to specific page, DEEPLINK_STORE - redirect to certain store, DEEPLINK_PRODUCT - deep link redirect to the product details, MESSAGE
  String? get data => throw _privateConstructorUsedError;
  String? get feature => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PushNotificationPayloadModelCopyWith<PushNotificationPayloadModel>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PushNotificationPayloadModelCopyWith<$Res> {
  factory $PushNotificationPayloadModelCopyWith(
          PushNotificationPayloadModel value,
          $Res Function(PushNotificationPayloadModel) then) =
      _$PushNotificationPayloadModelCopyWithImpl<$Res,
          PushNotificationPayloadModel>;
  @useResult
  $Res call({String type, String? data, String? feature});
}

/// @nodoc
class _$PushNotificationPayloadModelCopyWithImpl<$Res,
        $Val extends PushNotificationPayloadModel>
    implements $PushNotificationPayloadModelCopyWith<$Res> {
  _$PushNotificationPayloadModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? type = null,
    Object? data = freezed,
    Object? feature = freezed,
  }) {
    return _then(_value.copyWith(
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as String?,
      feature: freezed == feature
          ? _value.feature
          : feature // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PushNotificationPayloadModelCopyWith<$Res>
    implements $PushNotificationPayloadModelCopyWith<$Res> {
  factory _$$_PushNotificationPayloadModelCopyWith(
          _$_PushNotificationPayloadModel value,
          $Res Function(_$_PushNotificationPayloadModel) then) =
      __$$_PushNotificationPayloadModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String type, String? data, String? feature});
}

/// @nodoc
class __$$_PushNotificationPayloadModelCopyWithImpl<$Res>
    extends _$PushNotificationPayloadModelCopyWithImpl<$Res,
        _$_PushNotificationPayloadModel>
    implements _$$_PushNotificationPayloadModelCopyWith<$Res> {
  __$$_PushNotificationPayloadModelCopyWithImpl(
      _$_PushNotificationPayloadModel _value,
      $Res Function(_$_PushNotificationPayloadModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? type = null,
    Object? data = freezed,
    Object? feature = freezed,
  }) {
    return _then(_$_PushNotificationPayloadModel(
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as String?,
      feature: freezed == feature
          ? _value.feature
          : feature // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PushNotificationPayloadModel implements _PushNotificationPayloadModel {
  const _$_PushNotificationPayloadModel(
      {required this.type, this.data, this.feature});

  factory _$_PushNotificationPayloadModel.fromJson(Map<String, dynamic> json) =>
      _$$_PushNotificationPayloadModelFromJson(json);

  @override
  final String type;
// type - NOTIF - notif only no action, DEEPLINK - redirect to specific page, DEEPLINK_STORE - redirect to certain store, DEEPLINK_PRODUCT - deep link redirect to the product details, MESSAGE
  @override
  final String? data;
  @override
  final String? feature;

  @override
  String toString() {
    return 'PushNotificationPayloadModel(type: $type, data: $data, feature: $feature)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PushNotificationPayloadModel &&
            (identical(other.type, type) || other.type == type) &&
            (identical(other.data, data) || other.data == data) &&
            (identical(other.feature, feature) || other.feature == feature));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, type, data, feature);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PushNotificationPayloadModelCopyWith<_$_PushNotificationPayloadModel>
      get copyWith => __$$_PushNotificationPayloadModelCopyWithImpl<
          _$_PushNotificationPayloadModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PushNotificationPayloadModelToJson(
      this,
    );
  }
}

abstract class _PushNotificationPayloadModel
    implements PushNotificationPayloadModel {
  const factory _PushNotificationPayloadModel(
      {required final String type,
      final String? data,
      final String? feature}) = _$_PushNotificationPayloadModel;

  factory _PushNotificationPayloadModel.fromJson(Map<String, dynamic> json) =
      _$_PushNotificationPayloadModel.fromJson;

  @override
  String get type;
  @override // type - NOTIF - notif only no action, DEEPLINK - redirect to specific page, DEEPLINK_STORE - redirect to certain store, DEEPLINK_PRODUCT - deep link redirect to the product details, MESSAGE
  String? get data;
  @override
  String? get feature;
  @override
  @JsonKey(ignore: true)
  _$$_PushNotificationPayloadModelCopyWith<_$_PushNotificationPayloadModel>
      get copyWith => throw _privateConstructorUsedError;
}
