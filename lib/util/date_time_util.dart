
import 'package:intl/intl.dart';


class DateTimeUtil {

  static String getMonthDayYear(){
    var date = DateTime.now();
    return DateFormat("MMMM dd, yyyy").format(date);
  }

  static String getMonthYear(){
    var date = DateTime.now();
    return date.month.toString()+date.year.toString();
  }

  static String getStringDateNOW(){
    return DateTime.now().toIso8601String();
  }

  static String getFormattedDateDisplay(DateTime date){
    return DateFormat("MMMM dd, yyyy").format(date);
  }

  String timeIdentfier(DateTime time) {
    return time.hour >= 12 ? "PM" : "AM";
  }

}