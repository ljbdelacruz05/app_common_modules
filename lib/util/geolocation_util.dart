import 'package:app_dependency_manager/app_dependency_manager.dart';
import '../core/failure.dart';
import 'dart:math' as math;
import 'package:google_maps_webservice_ex/places.dart';
import 'package:google_maps_widget/google_maps_widget.dart';

import '../core/success.dart';



class GeolocationUtil {
  final GeolocatorPlatform _geoLocator;
  final GoogleMapsPlaces placesApiClient;

  GeolocationUtil(this._geoLocator, this.placesApiClient){
    _handlePermission();
  }

  Future<Either<Failure, LatLng>> getUserLocation() async{
    try{
      final position = await _geoLocator.getCurrentPosition();
      return Right(LatLng(position.longitude, position.latitude));
    }catch(e){
      return Left(LocationPermissionDeniedFailure());
    }
  }
  Future<Either<Failure, List<Prediction>>> fetchAddress(String address) async {
    try{
      Completer<Either<Failure, List<Prediction>>> completer = Completer();
      var result = await getUserLocation();
      result.fold((l) {
        completer.complete(Left(l));
      }, (r) async {
        PlacesAutocompleteResponse response = await placesApiClient.autocomplete(address, language: 'en', location: Location(lat: r.latitude, lng: r.longitude));
        completer.complete(Right(response.predictions));
      });
      return completer.future;
    }catch(e){
      return Left(GeoLocationUtilFailure());
    }
  }

  Future<Either<Failure, LatLng>> getLatLngOfPrediction(Prediction prediction) async {
    try{
      Completer<Either<Failure, LatLng>> completer = Completer();
      var result = await getPlaceDetails(prediction);
      result.fold((l){
        completer.complete(Left(l));
      }, (r){
          double latitude = r.result?.geometry?.location.lat ?? 0;
          double longitude = r.result?.geometry?.location.lng ?? 0;
          completer.complete(Right(LatLng(latitude, longitude)));
      });
      return completer.future;
    }catch(e){
      return Left(GeoLocationUtilFailure());
    }
  }

  Future<Either<Failure, PlacesDetailsResponse>> getPlaceDetails(Prediction prediction) async {
    try{
      PlacesDetailsResponse response =
        await placesApiClient.getDetailsByPlaceId(prediction.placeId ?? "");
        if (response.status == "OK") {
          return Right(response);
        } else {
          return Left(GeoLocationUtilFailure());
        }
    }catch(e){
      return Left(GeoLocationUtilFailure());
    }
  }

  

  Future<Either<Failure, Success>> _handlePermission() async {
    bool serviceEnabled;
    LocationPermission permission;
    // Test if location services are enabled.
    serviceEnabled = await _geoLocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Left(LocationServiceFailure());
    }
    permission = await _geoLocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await _geoLocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Left(LocationPermissionDeniedFailure());
      }
    }
    if (permission == LocationPermission.deniedForever) {
      return Left(LocationPermissionDeniedForeverFailure());
    }
    return Right(LocationPermissionGrantedSuccess());
  }


  // is within radius
  bool isWithinRadius(LatLng point1, LatLng point2, double radiusInKm) {
    const double earthRadius = 6371; // Earth's radius in km
    // Convert LatLng coordinates to radians
    double lat1Rad = _degreesToRadians(point1.latitude);
    double lon1Rad = _degreesToRadians(point1.longitude);
    double lat2Rad = _degreesToRadians(point2.latitude);
    double lon2Rad = _degreesToRadians(point2.longitude);

    // Calculate the difference in latitudes and longitudes
    double dLat = lat2Rad - lat1Rad;
    double dLon = lon2Rad - lon1Rad;

    // Haversine formula
    double a = math.pow(math.sin(dLat / 2), 2) +
        math.cos(lat1Rad) * math.cos(lat2Rad) * math.pow(math.sin(dLon / 2), 2);
    double c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a));
    double distance = earthRadius * c;
    return distance <= radiusInKm;
  }
  // Helper function to convert degrees to radians
  double _degreesToRadians(double degrees) {
    return degrees * math.pi / 180;
  }

  // lon1 - pick up, lon2 = delivery
  double _haversineDistance(double lat1, double lon1, double lat2, double lon2) {
    const R = 6371; // Earth's radius in kilometers

    double toRadians(double degree) {
      return degree * math.pi / 180;
    }

    double dLat = toRadians(lat2 - lat1);
    double dLon = toRadians(lon2 - lon1);

    lat1 = toRadians(lat1);
    lat2 = toRadians(lat2);

    double a = math.pow(math.sin(dLat / 2), 2) + math.pow(math.sin(dLon / 2), 2) * math.cos(lat1) * math.cos(lat2);
    double c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a));

    return R * c;
  }
  double calculateDeliveryFee(LatLng pickup, LatLng delivery, {double baseFee = 20.0, double pricePerKm = 48}) {
    var distance = _haversineDistance(pickup.latitude, pickup.longitude, delivery.latitude, delivery.longitude);
    return baseFee + (distance * pricePerKm);
  }
}