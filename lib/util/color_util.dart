import 'package:flutter/material.dart';

class ColorUtil {

  // THEME Light Mode
  Color lightTextColor;
  Color light2TextColor;
  Color lighTextColor3;
  Color lightTextColor5;
  Color lightBGColor;


  // THEME Dark Mode
  Color darkTextColor;
  Color dark2TextColor;
  Color darkBGCOlor;


  final bool isDarkMode;

  ColorUtil(
    this.isDarkMode,
    { 
    // THEME Light Mode
    this.lightTextColor = Colors.black,
    this.light2TextColor = Colors.white,
    this.lighTextColor3 = Colors.white,
    this.lightTextColor5 = Colors.grey,
    this.lightBGColor = Colors.white,

    // THEME Dark Mode
    this.darkTextColor = Colors.white,
    this.dark2TextColor = Colors.white,
    this.darkBGCOlor = Colors.black,
    });


    Color bgColor(){
      return isDarkMode ? darkBGCOlor : lightBGColor;
    }

    Color getTextColor(){
      return isDarkMode ? darkTextColor : lightTextColor;
    }
    
    Color getTextColor2(){
      return isDarkMode ? dark2TextColor : light2TextColor;
    }
    
    Color getTextColor3(){
      return isDarkMode ? dark2TextColor : lighTextColor3;
    }
    
    Color getTextColor4(){
      return isDarkMode ? dark2TextColor: light2TextColor;
    }
    
    Color getTextColor5(){
      return isDarkMode ? dark2TextColor: lightTextColor5;
    }
}