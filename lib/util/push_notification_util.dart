import 'package:app_dependency_manager/app_dependency_manager.dart';

import 'model/notification_model.dart';

class PushNotificationUtil {
  int id = 0;
  /// this push notif only accomodates show message
  void showPushNotif(String title, String message) async{
    id++;
    await AwesomeNotifications().createNotification(
        content: NotificationContent(
            id: id, // -1 is replaced by a random number
            channelKey: 'alerts',
            title: title,
            body: message,
            payload: {},
            notificationLayout: NotificationLayout.Default,
            ),
        );
  }

  void showPushNotifFromMessage(String title, String message, PushNotificationPayloadModel payload) async{
    id++;
    await AwesomeNotifications().createNotification(
        content: NotificationContent(
            id: id,
            channelKey: 'alerts',
            title: title,
            body: message,
            payload: payload.toJson().map((key, value) {
                        return MapEntry<String, String>(key, value.toString());
                    })
            ),
            actionButtons: [
              NotificationActionButton(
                  key: 'REPLY',
                  label: 'Reply Message',
                  requireInputText: true,
                  actionType: ActionType.SilentAction
              ),
              NotificationActionButton(
                  key: 'DISMISS',
                  label: 'Dismiss',
                  actionType: ActionType.DismissAction,
                  isDangerousOption: true)
            ]
        
        );
  }

}