


class StringUtil {
  String amountFormatter(double balance) {
    RegExp reg = new RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))');
    return balance.toStringAsFixed(2).replaceAllMapped(reg,  (Match match) => '${match[1]},');
  }

  String removeSpecCarFromMobile(String mobile, {String dialCode = "+63"}) {
    return mobile.contains(dialCode) ? mobile.replaceAll(dialCode, "0") : mobile;
  }

  bool isEmail(String input) {
    final pattern = r'^[\w-]+(\.[\w-]+)*@([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$';
    final regex = RegExp(pattern);
    return regex.hasMatch(input);
  }
}