
import 'package:dartz/dartz.dart';
import '../core/failure.dart';
import '../core/success.dart';



class ValidationUtil {

  Future<Either<Failure, Success>> validateEmail(String email) async {
    var result = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);
    if (result) {
      return Right(ValidationSuccess());
    } else {
      return Left(EmailValidationFailure());
    }
  }

 Future<Either<Failure, Success>> validatePhonenumber(
      String phonenumber) async {
    var result = RegExp(r'(^(09)\d{9}$)').hasMatch(phonenumber);
    if (result) {
      return Right(ValidationSuccess());
    } else {
      return Left(EmailValidationFailure());
    }
  }
  Future<Either<Failure, Success>> checkAmountIfZero(String amount) async{
    try{
      if(double.parse(amount) > 0){
        return Right(ValidationSuccess());
      } else {
        return Left(InvalidInputFailure());
      }
    } catch (e) {
      return Left(ParseAmountFailure());
    }
  }

  Future<Either<Failure, Success>> checkIfHasSpecialChar(String text) async {
    var result = RegExp(r"0-9").hasMatch(text);
    if (result) {
      return Right(ValidationSuccess());
    } else {
      return Left(InvalidInputFailure());
    }
  }
}