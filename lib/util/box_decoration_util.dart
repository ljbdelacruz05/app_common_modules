



import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BoxDecorationUtil {
  BoxDecoration borderAllDecorationRounded({Color borderColor = Colors.grey, double width = 1, double borderRadius = 12}){
    return BoxDecoration(
                  border: Border.all(
                    width: width,
                    color: borderColor
                  ),
                  borderRadius: BorderRadius.circular(borderRadius),
    );
  }
}