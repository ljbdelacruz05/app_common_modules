import 'package:flutter/material.dart';
import 'package:app_common_modules/features/core_widgets/core_widget_backgrounds.dart';
import 'package:app_common_modules/service_locator.dart';
import 'package:get/get.dart';

void main() {
  setupDefault();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ExamplePage(),
    );
  }
}

class ExamplePage extends StatelessWidget {
  final ExampleController controller = Get.put(ExampleController());
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CoreThemeBackgroundAtom(Text(controller.hasInternet ? "Has Internet" : "No Internet"));
  }
}


class ExampleController extends GetxController {
  bool hasInternet = false;
  ExampleController(){
    init();
  }
  init() async {
    hasInternet = await hasInternetConnection();
  }
}

