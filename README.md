Author: ljbdelacruz

# App Common Modules

this contains all the common dependencies like pre built UI and tools needed to build the application and focus on developing your features rather than building your own components.

## How to use
```
  app_common_modules:
    git:
      url: https://gitlab.com/ljbdelacruz05/app_common_modules.git 
      ref: custom_master (any branch from repo you want to use) 
```


# For Fix TODO

* Showcase Widget needs to be replaced since the dependencies we are using is deprecated had to remove it in order to work in latest flutter version


# CORE LIBRARY

## how to use the hive stream, shared preference stream 
```
  // my shared preferences sample usage:

  final String _keySharedPrefSample = "key_shared_preferences_sample";
  MySharedPreferences? _streamSharedPreferencesSample;
  _streamSharedPreferencesSample = MySharedPreferences(sharedPref)


  // this will set the value 
  _streamSharedPreferencesSample?.setValue(_keySharedPrefSample, "sample value here");
  // this will listen if new value has been added on this stream

  _streamSharedPreferencesSample?.valueStream.listen((event) { 
    // this function will be invoked when you set new value on this shared preferences
    // sample values here
    print(event);
  });

  // NOTE: define one stream listener like the code above ex. you want to listen to list of items in your cart changes that is equivalent to one shared pref sample if you want to listen to wallet balance changes you also need to define another _streamSharedPreferencesSample and another key value to it since you cannot reuse the existing shared pref sample since it is already dedicated to the cart items changes

  // hive stream initialization setup
  // this is if you are using web 
  import 'dart:html';
  var path = window.location.pathname;
  Hive.init(path)

  var hiveBox = await Hive.openBox('sample_storage')
  final String _keyHiveStreamSample = "key_hive_stream_sample";
  MyHiveStream? _streamHiveSample;
  _streamHiveSample = MyHiveStream(hiveBox);

  // sample implementation as the above sinc they are derived from same implementation instead of using shared preferences i just used HIVE

```

## Success, Failure extension

```
  // this is used if you want to have your custom error, success 

  // example custom failure category in your project

  import 'package:app_common_modules/core/failure.dart';

  class RemoteServiceFailure extends Failure {
    final String message;
    RemoteServiceFailure({this.message = ""});

    @override
    String getErrorMessage(){
      return "custom error message here";
    }
  }

  Future<Either<Failure, Success>> myCustomMethodFailure(){
    return Left(RemoteServiceFailure());
  }

  var customMethodResult = await myCustomMethodFailure();
  customMethodResult.fold((l){
    // this will print the 'custom error message here' defined on your custom failure 
    print(l.getErrorMessage());
  }, (r)=>null);

  // example for success you can also define your own custom parameters for the extended class success or in failure 
  // purpose is this is for more clarity of what you will be expecting 

  import 'package:app_common_modules/core/success.dart';

  class RegisterResendOTPSuccess extends Success {
    final String email;
    RegisterResendOTPSuccess(this.email);
  }
```


# Features Library
[Features Readme](lib/features/featuresREADME.md)

# Utility Library
[Utility Readme](lib/util/utilREADME.md)





